﻿using System.ComponentModel.DataAnnotations;

namespace Dominio.Pottencial.DTO
{
    public class ItemsDTO
    {
        [Display(Description = "Quantidade")]
        [Range(1, int.MaxValue, ErrorMessage = "Quantidade de itens é informação obrigatória !")]
        public int Quantidade { get; set; }

        [Display(Description = "Nome do produto")]
        [Required(ErrorMessage = "Nome do produto é informação obrigatória !")]
        public string NomeDoProduto { get; set; }

        [Display(Description = "Codigo Produto")]
        [Range(1, int.MaxValue, ErrorMessage = "Quantidade de itens é informação obrigatória !")]
        public int Id { get; set; }

    }
}

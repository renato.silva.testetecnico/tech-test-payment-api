﻿using System.ComponentModel.DataAnnotations;

namespace Dominio.Pottencial.DTO
{
    public class VendasDTO
    {
        [Display(Description = "Dados do vendedor")]
        [Required(ErrorMessage = "Dados do vendedor é informação obrigatória !")]
        public VendedorDTO Vendedor { get; set; }

        [Display(Description = "Itens da venda")]
        [Required(ErrorMessage = "Não é possível registrar a venda sem nenhum item !")]
        public List<ItemsDTO> ItensDaVenda { get; set; }

        public int Id { get; set; }
    }
}

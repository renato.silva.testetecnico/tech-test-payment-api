﻿using Auxiliar.Pottencial.Enums;
using System.ComponentModel.DataAnnotations;

namespace Dominio.Pottencial.DTO
{
    public class AtualizarStatusDaVendaDTO
    {
        [Required]
        public int IdVenda { get; set; }

        [Required]
        public StatusDaVenda NovoStatus { get; set; }
    }
}

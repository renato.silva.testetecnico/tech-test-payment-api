﻿using System.ComponentModel.DataAnnotations;

namespace Dominio.Pottencial.DTO
{
    public class VendedorDTO
    {
        [Required(ErrorMessage = "E-mail é informação obrigatória !")]
        public string Email { get; set; }

        [Display(Description = "Nome do Vendedor")]
        [Required(ErrorMessage = "Nome do vendedor é informação obrigatória !")]
        public string Nome { get; set; }

        [Display(Description = "Celular do Vendedor")]
        [Required(ErrorMessage = "Número do celular do vendedor é informação obrigatória !")]
        public string Celular { get; set; }

        [Required(ErrorMessage = "CPF do vendedor é informação obrigatória !")]
        public string CPF { get; set; }
        public int Id { get; set; }
    }
}

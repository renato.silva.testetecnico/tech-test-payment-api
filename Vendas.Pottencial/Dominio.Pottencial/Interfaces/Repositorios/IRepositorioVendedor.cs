﻿using Dominio.Pottencial.Entidades;

namespace Dominio.Pottencial.Interfaces.Repositorios
{
    public interface IRepositorioVendedor
    {
        Vendedor Buscar(int id, params string[] includes);
    }
}

﻿using Dominio.Pottencial.Entidades;

namespace Dominio.Pottencial.Interfaces.Repositorios
{
    public interface IRepositorioItensDaVenda
    {
        Item Buscar(int id, params string[] includes);
    }
}

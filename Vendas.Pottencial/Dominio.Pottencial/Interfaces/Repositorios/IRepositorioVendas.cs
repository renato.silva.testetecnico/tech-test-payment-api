﻿using Dominio.Pottencial.Entidades;

namespace Dominio.Pottencial.Interfaces.Repositorios
{
    public interface IRepositorioVendas
    {
        void Incluir(Venda entidade);
        Venda Buscar(int id);
        void Atualizar(Venda entidade);
    }
}

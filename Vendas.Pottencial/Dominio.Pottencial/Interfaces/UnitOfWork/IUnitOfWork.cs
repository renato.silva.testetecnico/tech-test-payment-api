﻿namespace Dominio.Pottencial.Interfaces.UnitOfWork
{
    public interface IUnitOfWork
    {
        void Commit();
        void Rollback();
    }
}

﻿using Dominio.Pottencial.DTO;

namespace Dominio.Pottencial.Interfaces.Servicos
{
    public interface IRegistrarVendasService
    {
        bool Registrar(VendasDTO vendaRecebida);
    }
}

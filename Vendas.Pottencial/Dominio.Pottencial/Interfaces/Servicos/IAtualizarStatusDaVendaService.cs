﻿using Dominio.Pottencial.DTO;

namespace Dominio.Pottencial.Interfaces.Servicos
{
    public interface IAtualizarStatusDaVendaService
    {
        bool Atualizar(AtualizarStatusDaVendaDTO atualizarStatus);
    }
}

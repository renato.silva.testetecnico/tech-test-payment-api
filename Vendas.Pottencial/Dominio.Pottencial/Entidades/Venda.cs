﻿using Auxiliar.Pottencial.Enums;

namespace Dominio.Pottencial.Entidades
{
    public partial class Venda
    {
        public Venda()
        {
            Items = new HashSet<Item>();
        }

        public int Id { get; set; }
        public Guid NumeroDoPedido { get; set; }
        public DateTime DataDaOperacao { get; set; }
        public int IdVendedor { get; set; }
        public StatusDaVenda StatusDaVenda { get; set; }

        public virtual Vendedor Vendedor { get; set; } = null!;
        public virtual ICollection<Item> Items { get; set; }
    }
}

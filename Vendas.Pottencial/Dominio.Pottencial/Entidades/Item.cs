﻿namespace Dominio.Pottencial.Entidades
{
    public partial class Item
    {
        public int Id { get; set; }
        public int Quantidade { get; set; }
        public string NomeDoProduto { get; set; } = null!;
        public int? IdVenda { get; set; }

        public virtual Venda? Venda { get; set; }
    }
}

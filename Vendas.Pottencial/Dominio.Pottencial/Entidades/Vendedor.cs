﻿namespace Dominio.Pottencial.Entidades
{
    public partial class Vendedor
    {
        public Vendedor()
        {
            Venda = new HashSet<Venda>();
        }

        public int Id { get; set; }
        public string Email { get; set; } = null!;
        public string Nome { get; set; } = null!;
        public string Celular { get; set; } = null!;
        public string Cpf { get; set; } = null!;

        public virtual ICollection<Venda> Venda { get; set; }
    }
}

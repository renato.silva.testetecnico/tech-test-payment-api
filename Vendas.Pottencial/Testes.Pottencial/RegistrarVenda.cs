using Aplicacao.Pottencial.Servicos;
using Auxiliar.Pottencial.Excecoes;
using Dominio.Pottencial.DTO;
using Dominio.Pottencial.Entidades;
using Dominio.Pottencial.Interfaces.Repositorios;
using Dominio.Pottencial.Interfaces.UnitOfWork;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace Testes.Pottencial
{
    [TestClass]
    public class Tests
    {
        IUnitOfWork mockUnitOfWork;
        IRepositorioVendas mockRepositorioVenda;
        IRepositorioVendedor mockRepositorioVendedor;

        [SetUp]
        public void Setup()
        {
            mockUnitOfWork = Substitute.For<IUnitOfWork>();
            mockRepositorioVenda = Substitute.For<IRepositorioVendas>();
            mockRepositorioVendedor = Substitute.For<IRepositorioVendedor>();
        }

        [Test]
        public void TestarVendaNula()
        {
            var venda = new VendasDTO
            {
                Vendedor = new VendedorDTO { Id = -1 },
                ItensDaVenda = new List<ItemsDTO>() { new ItemsDTO() }
            };

            var servico = new RegistrarVendasService(mockUnitOfWork, mockRepositorioVenda, mockRepositorioVendedor);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.ThrowsException<NaoLocalizadoException>(() => servico.Registrar(venda));
            mockUnitOfWork.DidNotReceiveWithAnyArgs().Commit();
        }


        [Test]
        public void TestarListaItensVaziaa()
        {
            var venda = new VendasDTO
            {
                Vendedor = new VendedorDTO { Id = 1 },
                ItensDaVenda = new List<ItemsDTO>() { }
            };

            mockRepositorioVendedor.Buscar(Arg.Any<int>()).Returns(new Vendedor { Nome = "Vendedor 1" });

            var servico = new RegistrarVendasService(mockUnitOfWork, mockRepositorioVenda, mockRepositorioVendedor);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.ThrowsException<ValidacaoException>(() => servico.Registrar(venda));
            mockUnitOfWork.DidNotReceiveWithAnyArgs().Commit();
        }


    }
}
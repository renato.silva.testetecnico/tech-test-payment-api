﻿namespace Auxiliar.Pottencial.Excecoes
{
    public abstract class BadRequestException : Exception
    {
        protected BadRequestException(string message) : base(message) { }
    }
}

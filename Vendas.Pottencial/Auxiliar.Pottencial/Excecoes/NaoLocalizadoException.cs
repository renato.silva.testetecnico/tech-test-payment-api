﻿using System.Runtime.Serialization;

namespace Auxiliar.Pottencial.Excecoes
{
    public class NaoLocalizadoException : Exception
    {
        public NaoLocalizadoException() { }
        public NaoLocalizadoException(string message) : base(message) { }
        public NaoLocalizadoException(string message, Exception innerException) : base(message, innerException) { }
        protected NaoLocalizadoException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}

﻿namespace Auxiliar.Pottencial.Excecoes
{
    public sealed class NaoPermitidoException : Exception
    {
        public NaoPermitidoException(string msg) : base(msg) { }
    }
}

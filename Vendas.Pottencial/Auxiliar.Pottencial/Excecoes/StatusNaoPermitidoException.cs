﻿using System.Runtime.Serialization;

namespace Auxiliar.Pottencial.Excecoes
{
    public class StatusNaoPermitidoException : Exception
    {
        public StatusNaoPermitidoException() { }

        public StatusNaoPermitidoException(string message) : base(message) { }

        public StatusNaoPermitidoException(string message, Exception innerException) : base(message, innerException) { }

        protected StatusNaoPermitidoException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}

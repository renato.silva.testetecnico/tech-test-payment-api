﻿namespace Auxiliar.Pottencial.Excecoes
{
    public sealed class ValidacaoException : BadRequestException
    {
        public ValidacaoException(string message) : base(message) { }
    }
}

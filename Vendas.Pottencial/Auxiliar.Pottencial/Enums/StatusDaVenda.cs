﻿using System.ComponentModel.DataAnnotations;

namespace Auxiliar.Pottencial.Enums
{
    public enum StatusDaVenda : byte
    {
        [Display(Name = "Aguardando pagamento")]
        AguardandoPagamento,

        [Display(Name = "Pagamento aprovado")]
        PagamentoAprovado,

        [Display(Name = "Enviado para transportadora")]
        EnviadoParaTransportadora,

        [Display(Name = "Entregue")]
        Entregue,

        [Display(Name = "Cancelada")]
        Cancelada
    }
}

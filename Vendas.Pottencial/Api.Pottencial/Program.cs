using Aplicacao.Pottencial.Servicos;
using Dados.Pottencial;
using Dados.Pottencial.Repositorios;
using Dominio.Pottencial.Interfaces.Repositorios;
using Dominio.Pottencial.Interfaces.Servicos;
using Dominio.Pottencial.Interfaces.UnitOfWork;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<VendasPottencialContext>(o => o.UseInMemoryDatabase("Vendas-Pottencial"));

builder.Services.AddScoped<IRegistrarVendasService, RegistrarVendasService>();
builder.Services.AddScoped<IAtualizarStatusDaVendaService, AtualizarStatusDaVendaService>();

builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();

builder.Services.AddScoped<IRepositorioVendas, RepositorioVendas>();
builder.Services.AddScoped<IRepositorioItensDaVenda, RepositorioItensDaVenda>();
builder.Services.AddScoped<IRepositorioVendedor, RepositorioVendedor>();


var app = builder.Build();



// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api.Pottencial v1");
        c.RoutePrefix = "api-docs";
    });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

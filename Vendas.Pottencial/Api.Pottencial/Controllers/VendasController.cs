﻿using Dominio.Pottencial.DTO;
using Dominio.Pottencial.Entidades;
using Dominio.Pottencial.Interfaces.Repositorios;
using Dominio.Pottencial.Interfaces.Servicos;
using Microsoft.AspNetCore.Mvc;


namespace Api.Pottencial.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendasController : ControllerBase
    {

        private readonly IRepositorioVendas _repositorioVendas;
        private readonly IRegistrarVendasService _registrarVendasService;
        private readonly IAtualizarStatusDaVendaService _atualizarStatusDaVendaService;


        public VendasController(
                                            IRepositorioVendas repositorioVendas,
                                            IRegistrarVendasService registrarVendasService,
                                            IAtualizarStatusDaVendaService atualizarStatusDaVendaService
                                        )
        {
            _repositorioVendas = repositorioVendas;
            _registrarVendasService = registrarVendasService;
            _atualizarStatusDaVendaService = atualizarStatusDaVendaService;
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<Venda> BuscarVendasPorId([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var retorno = _repositorioVendas.Buscar(id);
            if (retorno == null)
                return NotFound("Venda não localizada !");
            return Ok(retorno);
        }

        [HttpPost]
        public ActionResult<bool> RegistrarNovaVenda([FromBody] VendasDTO venda)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var retorno = _registrarVendasService.Registrar(venda);
            return Ok(retorno);
        }

        [HttpPut]
        public ActionResult<bool> AtualizarStatusDaVenda([FromBody] AtualizarStatusDaVendaDTO atualizarStatusDaVenda)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var retorno = _atualizarStatusDaVendaService.Atualizar(atualizarStatusDaVenda);
            return Ok(retorno);
        }
    }
}

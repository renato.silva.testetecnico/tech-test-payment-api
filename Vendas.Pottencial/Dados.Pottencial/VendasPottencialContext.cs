﻿using Dominio.Pottencial.Entidades;
using Microsoft.EntityFrameworkCore;

namespace Dados.Pottencial
{
    public partial class VendasPottencialContext : DbContext
    {
        public VendasPottencialContext()
        {
        }

        public VendasPottencialContext(DbContextOptions<VendasPottencialContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Item> Items { get; set; } = null!;
        public virtual DbSet<Venda> Vendas { get; set; } = null!;
        public virtual DbSet<Vendedor> Vendedor { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=DESK01\\SQLEXPRESS;Initial Catalog=Vendas-Pottencial;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>(entity =>
            {
                entity.ToTable("Item");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.NomeDoProduto).IsUnicode(false);

                entity.HasOne(d => d.Venda)
                    .WithMany(p => p.Items)
                    .HasForeignKey(d => d.IdVenda)
                    .HasConstraintName("FK_ItensDaVenda_Venda");
            });

            modelBuilder.Entity<Venda>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.DataDaOperacao).HasColumnType("datetime");

                entity.HasOne(d => d.Vendedor)
                    .WithMany(p => p.Venda)
                    .HasForeignKey(d => d.IdVendedor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Vendas_Vendedor");
            });

            modelBuilder.Entity<Vendedor>(entity =>
            {
                entity.ToTable("Vendedor");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Celular).IsUnicode(false);

                entity.Property(e => e.Cpf)
                    .IsUnicode(false)
                    .HasColumnName("CPF");

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

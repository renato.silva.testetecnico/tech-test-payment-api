﻿using Dominio.Pottencial.Interfaces.UnitOfWork;

namespace Dados.Pottencial
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly VendasPottencialContext _vendasContexto;

        public UnitOfWork(VendasPottencialContext vendasContexto)
        {
            _vendasContexto = vendasContexto;
        }

        public void Commit()
        {
            _vendasContexto.SaveChanges();
        }

        public void Dispose()
        {
            if (_vendasContexto != null)
                _vendasContexto.Dispose();
        }

        public void Rollback()
        {
            Dispose();
        }
    }
}

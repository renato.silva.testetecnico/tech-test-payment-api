﻿using Dominio.Pottencial.Entidades;
using Dominio.Pottencial.Interfaces.Repositorios;

namespace Dados.Pottencial.Repositorios
{
    public class RepositorioVendedor : IRepositorioVendedor
    {
        private readonly VendasPottencialContext _context;

        public RepositorioVendedor(VendasPottencialContext context)
        {
            _context = context;
        }

        public Vendedor Buscar(int id, params string[] includes)
        {
            return _context.Vendedor.Where(a => a.Id == id).FirstOrDefault();
        }
    }
}

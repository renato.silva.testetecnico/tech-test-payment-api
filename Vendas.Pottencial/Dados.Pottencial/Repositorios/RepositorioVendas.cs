﻿using Dominio.Pottencial.Entidades;
using Dominio.Pottencial.Interfaces.Repositorios;

namespace Dados.Pottencial.Repositorios
{
    public class RepositorioVendas : IRepositorioVendas
    {
        private readonly VendasPottencialContext _context;

        public RepositorioVendas(VendasPottencialContext context)
        {
            _context = context;
        }

        public void Atualizar(Venda entidade)
        {
            _context.Vendas.Update(entidade);
        }

        public Venda Buscar(int id)
        {
            return _context.Vendas.Where(a => a.Id == id)
                        .Select(a => new Venda
                        {
                            Id = a.Id,
                            NumeroDoPedido = a.NumeroDoPedido,
                            DataDaOperacao = a.DataDaOperacao,
                            IdVendedor = a.IdVendedor,
                            StatusDaVenda = a.StatusDaVenda,
                            Items = a.Items,
                            Vendedor = a.Vendedor
                        }).FirstOrDefault();
        }

        public void Incluir(Venda entidade)
        {
            _context.Vendas.Add(entidade);
        }
    }
}

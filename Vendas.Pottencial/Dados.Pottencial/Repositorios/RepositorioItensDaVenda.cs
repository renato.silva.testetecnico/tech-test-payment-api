﻿using Dominio.Pottencial.Entidades;
using Dominio.Pottencial.Interfaces.Repositorios;

namespace Dados.Pottencial.Repositorios
{
    public class RepositorioItensDaVenda : IRepositorioItensDaVenda
    {
        private readonly VendasPottencialContext _context;
        public RepositorioItensDaVenda(VendasPottencialContext context)
        {
            _context = context;
        }

        public Item Buscar(int id, params string[] includes)
        {
            return _context.Items.Where(a => a.Id == id).FirstOrDefault();
        }
    }
}

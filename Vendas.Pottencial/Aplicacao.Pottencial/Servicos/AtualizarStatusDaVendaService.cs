﻿using Auxiliar.Pottencial.Excecoes;
using Dominio.Pottencial.DTO;
using Dominio.Pottencial.Interfaces.Repositorios;
using Dominio.Pottencial.Interfaces.Servicos;
using Dominio.Pottencial.Interfaces.UnitOfWork;

namespace Aplicacao.Pottencial.Servicos
{
    public class AtualizarStatusDaVendaService : IAtualizarStatusDaVendaService
    {

        private readonly IRepositorioVendas _repositorioVendas;
        private readonly IUnitOfWork _unitOfWork;

        public AtualizarStatusDaVendaService(IRepositorioVendas repositorioVendas, IUnitOfWork unitOfWork)
        {
            _repositorioVendas = repositorioVendas;
            _unitOfWork = unitOfWork;
        }

        public bool Atualizar(AtualizarStatusDaVendaDTO atualizarStatus)
        {

            try
            {
                var venda = _repositorioVendas.Buscar(atualizarStatus.IdVenda);

                if (venda == null)
                    throw new NaoLocalizadoException("Venda não localizada !");

                ValidarStatusDaVenda.Validar(venda.StatusDaVenda, atualizarStatus.NovoStatus);

                venda.StatusDaVenda = atualizarStatus.NovoStatus;
                _repositorioVendas.Atualizar(venda);

                _unitOfWork.Commit();

                return true;
            }
            catch
            {
                _unitOfWork.Rollback();
                throw;
            }
        }

    }
}

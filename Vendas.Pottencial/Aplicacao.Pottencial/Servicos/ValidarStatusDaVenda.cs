﻿using Auxiliar.Pottencial.Enums;
using Auxiliar.Pottencial.Excecoes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacao.Pottencial.Servicos
{
    internal static class ValidarStatusDaVenda
    {
        public static void Validar(StatusDaVenda statusAtual, StatusDaVenda novoStatus)
        {
            switch (statusAtual)
            {
                case StatusDaVenda.AguardandoPagamento:
                    if (novoStatus != StatusDaVenda.PagamentoAprovado && novoStatus != StatusDaVenda.Cancelada)
                        throw new NaoPermitidoException("Atualização de status negada !");
                    break;
                case StatusDaVenda.PagamentoAprovado:
                    if (novoStatus != StatusDaVenda.EnviadoParaTransportadora && novoStatus != StatusDaVenda.Cancelada)
                        throw new NaoPermitidoException("Atualização de status negada !");
                    break;
                case StatusDaVenda.EnviadoParaTransportadora:
                    if (novoStatus != StatusDaVenda.Entregue)
                        throw new NaoPermitidoException("Atualização de status negada !");
                    break;
                case StatusDaVenda.Entregue:
                case StatusDaVenda.Cancelada:
                default:
                    throw new StatusNaoPermitidoException("Atualização de status negada !");
                    break;
            }
        }
    }
}

﻿using Auxiliar.Pottencial.Enums;
using Auxiliar.Pottencial.Excecoes;
using Dominio.Pottencial.DTO;
using Dominio.Pottencial.Entidades;
using Dominio.Pottencial.Interfaces.Repositorios;
using Dominio.Pottencial.Interfaces.Servicos;
using Dominio.Pottencial.Interfaces.UnitOfWork;

namespace Aplicacao.Pottencial.Servicos
{
    public class RegistrarVendasService : IRegistrarVendasService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepositorioVendas _repositorioVendas;
        private readonly IRepositorioVendedor _repositorioVendedor;

        public RegistrarVendasService(
                                        IUnitOfWork unitOfWork,
                                        IRepositorioVendas repositorioVendas,
                                        IRepositorioVendedor repositorioVendedor
                                   )
        {
            _unitOfWork = unitOfWork;
            _repositorioVendas = repositorioVendas;
            _repositorioVendedor = repositorioVendedor;
        }

        public bool Registrar(VendasDTO vendaRecebida)
        {
            try
            {
                Vendedor vendedor = BuscarVendedor(vendaRecebida);

                if (!vendaRecebida.ItensDaVenda.Any())
                    throw new ValidacaoException("Não é possível incluir uma Venda sem nenhum item.");

                var listaDeItens = MontaLista(vendaRecebida);

                var venda = new Venda
                {
                    Id = vendaRecebida.Id,
                    DataDaOperacao = DateTime.Now,
                    NumeroDoPedido = Guid.NewGuid(),
                    StatusDaVenda = StatusDaVenda.AguardandoPagamento,
                    Vendedor = vendedor,
                    Items = listaDeItens
                };

                _repositorioVendas.Incluir(venda);

                _unitOfWork.Commit();

                return true;
            }
            catch (Exception ex)
            {
                _unitOfWork.Rollback();
                throw ex;
            }
        }

        private List<Item> MontaLista(VendasDTO vendaRecebida)
        {
            var listaDeItens = new List<Item>();
            foreach (var item in vendaRecebida.ItensDaVenda)
            {
                listaDeItens.Add(new Item
                {
                    Id = item.Id,
                    NomeDoProduto = item.NomeDoProduto,
                    Quantidade = item.Quantidade
                });
            }
            return listaDeItens;
        }

        private Vendedor BuscarVendedor(VendasDTO vendaRecebida)
        {
            var vendedor = _repositorioVendedor.Buscar(vendaRecebida.Vendedor.Id);

            if (vendedor != null)
                return vendedor;

            vendedor = new Vendedor
            {
                Id = vendaRecebida.Vendedor.Id,
                Cpf = vendaRecebida.Vendedor.CPF,
                Celular = vendaRecebida.Vendedor.Celular,
                Email = vendaRecebida.Vendedor.Email,
                Nome = vendaRecebida.Vendedor.Nome
            };
            return vendedor;
        }
    }
}
